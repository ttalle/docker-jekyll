# docker-jekyll: Jekyll build docker image with NodeJS

Based on the (full) Ruby image with build-essentials and NodeJS

## Docker Container

The Dockerfile builds a container very close to the vanilla Ruby docker image.

### Docker

Get the latest image using docker.

```
$ docker run -ti --rm ...:latest
```

### Manual builds

You can make a local build using the provided buildscript:

```
./build.sh
```
