# Jekyll build docker image
#
# Based on the (full) Ruby image with build-essentials and custom Node install
#
# see: https://hub.docker.com/_/node/?tab=description
# and: https://github.com/nodejs/docker-node

FROM ruby:3-bookworm

MAINTAINER "Talle <talle-gitlab@gelesneeuw.nl>"

# Build essentials and package helpers for native node modules
RUN apt-get update && apt-get install -y --no-install-recommends \
		bash \
		build-essential \
		g++ \
		gcc \
		git \
		libc6-dev \
		make \
		openssh-client \
		pkg-config \
		rsync \
		ca-certificates \
		curl \
		gnupg \
		brotli \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/*

# As defined in the Node Docker image
#USER node:node

# Install NodeJS (20 LTS)
ENV NODE_MAJOR=20

RUN mkdir -p /etc/apt/keyrings \
	&& curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg \
	&& echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list \
	&& apt-get update \
	&& apt-get install nodejs -y \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/* \
	&& node --version \
	&& npm --version

# The official Ruby image sets the Bundle config
# to use a global gem cache... 
# 
# https://hub.docker.com/_/ruby
#ENV GEM_HOME=_vendor/bundle
#ENV BUNDLE_PATH=
#ENV BUNDLE_BIN=
#ENV BUNDLE_SILENCE_ROOT_WARNING=1
#ENV BUNDLE_APP_CONFIG=

# We set a dummy git config because ng new tries to commit everything
# to git unless you use the --skip-git flag (warning, this won't create
# a proper .gitignore either).
# see: https://github.com/angular/angular-cli/issues/4854
RUN git config --global user.email "jekyll-docker@gelesneeuw.nl" \
	&& git config --global user.name "Jekyll Docker"

VOLUME /usr/src/app

WORKDIR /usr/src/app

CMD ["bundle", "--help"]
