#!/usr/bin/env bash

DOCKER_CONTAINER="docker/docker-jekyll"
DOCKER_TAG="latest"
DOCKER_IMAGE="${DOCKER_CONTAINER}:${DOCKER_TAG}"

docker build -t "$DOCKER_IMAGE" .
